# How to run the project locally
## Getting started
In order to run the project locally you have to have [Node.js](https://nodejs.org/en/) installed on your device.\
Click the link and download LTS version.

## Navigate to the project's directory
Using the command line (e.g. embedded terminal in Visual Studio Code) navigate to the location of the project.\
Use the command "cd" followed by the absoulute path. For example:
```sh
cd C:\WEB-Projects\tea-code-app
```

## Install dependencies
You need to install all necessary dependencies. In order to do that type "npm install" in the command line and wait. It may take a while. :)
```sh
npm install
```

## Run the app
Finally type the command "npm start" to run the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
```sh
npm start
```
